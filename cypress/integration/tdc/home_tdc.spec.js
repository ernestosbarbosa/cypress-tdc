/// <reference types="Cypress" />

describe('Página do TDC', () => {

    beforeEach(() => {
        cy.visit("http://thedevelopersconference.com.br");
    })

    it('Dias TDC POA', () => {
        cy.get('.edition-card a[href="/tdc/2019/portoalegre/trilhas"]').click();
        cy.get('a[href="/tdc/2019/portoalegre/trilha-devtest"]')
            .should('contain.text', 'DEVTEST');
    })

})